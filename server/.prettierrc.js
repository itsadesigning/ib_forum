module.exports = {
	semi: true,
	trailingComma: 'all',
	singleQuote: true,
	tabWidth: 2,
	arrowParens: 'avoid',
	bracketSpacing: true,
	endOfLine: 'auto',
	htmlWhitespaceSensitivity: 'css',
	jsxBracketSameLine: true,
	jsxSingleQuote: true,
	printWidth: 120,
};
