import auth from './auth/router';
import users from './api/users/router';
import questions from './api/questions/router';
import answers from './api/answers/router';
import rating from './api/rating/router';
import AppError from './common/utils/app-exception';
import { NOT_FOUND } from './common/utils/exception-codes';

import { ReqResNextCallback } from './common/types';

const notFound: ReqResNextCallback = (req, res, next) => {
	next(new AppError(NOT_FOUND, NOT_FOUND.message(req.originalUrl)));
};

export default {
	auth,
	users,
	questions,
	answers,
	rating,
	notFound,
};
