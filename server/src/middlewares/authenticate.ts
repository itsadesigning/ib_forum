import catchAsync from '../common/utils/catch-async';
import AppException from '../common/utils/app-exception';

import jwt from '../common/config/jwt';

const protect = catchAsync(async (req, res, next) => {
	const accessToken = jwt.trimAuthHeaders(req.headers.authorization);
	if (!accessToken) return next(new AppException({ httpCode: 401 }, 'Not authorized to acces this route!'));

	if (jwt.checkIsBlacklisted(accessToken))
		return next(new AppException({ httpCode: 404 }, 'This token is no longer in use!'));

	const decoded = await jwt.verify(accessToken);

	res.locals.user = decoded.result;
	res.locals.accessToken = accessToken;

	next();
});

export default protect;
