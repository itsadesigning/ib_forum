import env from '../common/config/env';

import { ErrCallback } from '../common/types';

const environmentErrors = {
	development: (err, res) => {
		res.status(err.httpCode).json({
			status: err.status,
			message: err.message,
			code: err.code,
			details: err.details,
			...err,
		});
	},
	production: (err, res) => {
		if (err.isOperational) {
			res.status(err.httpCode).json({
				status: err.status,
				message: err.message,
				code: err.code,
			});
		} else {
			res.status(505).json({
				status: 'error',
				message: 'Something went wrong!',
			});
		}
	},
};

const errorHandler: ErrCallback = (err, req, res, next) => {
	err.httpCode = err.httpCode || 500;
	err.status = err.status || 'error';

	if (environmentErrors[env.environment]) {
		return environmentErrors[env.environment] && environmentErrors[env.environment](err, res);
	}

	res.status(err.httpCode).json({
		status: err.status,
		message: err.message,
	});
};

export default errorHandler;
