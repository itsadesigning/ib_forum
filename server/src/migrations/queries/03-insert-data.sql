INSERT INTO forum_user (
  "firstName",
  "lastName",
  "email",
  "password",
  "verificationCode"
) VALUES 
  ('Ivan', 'Bogdan', 'bogdan.ivan@example.com', 'test123', '00000000-0000-0000-0000-000000000000'), 
  ('Ivica', 'Bogdan', 'bogdan.ivica@example.com', 'test123', '00000000-0000-0000-0000-000000000000');

INSERT INTO 
  forum_question (
    "userId",
    "content"
  ) 
VALUES
  (1, 'How you doin?'), 
  (2, 'Whatssaaap?!');

INSERT INTO 
  forum_answer (
    "questionId",
    "userId",
    "content"
  ) 
VALUES 
  (1, 2, 'Not much, u?'), 
  (2, 2, 'Not a thing...');
