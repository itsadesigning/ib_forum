CREATE TABLE IF NOT EXISTS forum_user (
  "id" INT GENERATED ALWAYS AS IDENTITY,
  "firstName" VARCHAR(50) NOT NULL, 
  "lastName" VARCHAR(50) NOT NULL,
  "email" VARCHAR(50) NOT NULL,
  "password" VARCHAR(60) NOT NULL,
  "verified" BOOLEAN DEFAULT false,
  "verificationCode" VARCHAR(36) NOT NULL,
  "notification" BOOLEAN DEFAULT false,
  "createdAt" TIMESTAMP DEFAULT NOW(),
  "updatedAt" TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY("id"),
  CONSTRAINT unq_forum_user UNIQUE("email")
);

CREATE TABLE IF NOT EXISTS forum_question (
  "id" INT GENERATED ALWAYS AS IDENTITY,
  "userId" INT,
  "content" VARCHAR(500) NOT NULL, 
  "rating" SMALLINT DEFAULT 0,
  "createdAt" TIMESTAMP DEFAULT NOW(),
  "updatedAt" TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY("id"),
  CONSTRAINT fk_question_user
    FOREIGN KEY("userId")
      REFERENCES forum_user("id")
      ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS forum_question_rating (
  "userId" INT,
  "questionId" INT,
  "value" BOOLEAN NOT NULL,
  CONSTRAINT fk_rating_user
    FOREIGN KEY("userId")
      REFERENCES forum_user("id")
      ON DELETE CASCADE,
  CONSTRAINT fk_rating_question 
    FOREIGN KEY("questionId")
      REFERENCES forum_question("id")
      ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS forum_answer (
  "id" INT GENERATED ALWAYS AS IDENTITY,
  "questionId" INT,
  "userId" INT,
  "content" VARCHAR(500),
  "rating" SMALLINT DEFAULT 0,
  "createdAt" TIMESTAMP DEFAULT NOW(),
  "updatedAt" TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY("id"),
  CONSTRAINT fk_answer_user
    FOREIGN KEY("userId")
      REFERENCES forum_user("id")
      ON DELETE CASCADE,
  CONSTRAINT fk_answer_question
    FOREIGN KEY("questionId")
      REFERENCES forum_question("id")
      ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS forum_answer_rating (
  "userId" INT,
  "answerId" INT,
  "value" BOOLEAN NOT NULL,
  CONSTRAINT fk_rating_user
    FOREIGN KEY("userId")
      REFERENCES forum_user("id")
      ON DELETE CASCADE,
  CONSTRAINT fk_rating_answer
    FOREIGN KEY("answerId")
      REFERENCES forum_answer("id")
      ON DELETE CASCADE
);
