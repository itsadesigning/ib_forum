import fs from 'fs';
import minimist from 'minimist';

import loadQuery from '../common/utils/load-query';
import psql from '../common/db/psql';

const executeQuery = async query => {
	return new Promise<string>(res => {
		psql.query(query, err => {
			if (err) throw err;
			res(query);
		});
	});
};

const listFiles = async () => {
	return new Promise<string[]>(res => {
		fs.readdir(__dirname + '/queries', (err, files) => {
			res(files);
		});
	});
};

const migrate = async () => {
	const args = minimist(process.argv.slice(2));
	const filePath = file => `${__dirname}/queries/${file}`;

	try {
		if (!args.f) {
			const files = await listFiles();
			for (const file of files) await executeQuery(await loadQuery(filePath(file)));
		} else {
			await executeQuery(args.f);
		}
	} catch (err) {
		console.error(err);
		process.exit(1);
	}
};

migrate();
