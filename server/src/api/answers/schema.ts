import Joi from 'joi';

export const schema = Joi.object({
	userId: Joi.number().required(),
	content: Joi.string().alphanum().min(3).max(500).required(),
});
