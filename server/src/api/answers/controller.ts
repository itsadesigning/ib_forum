import path from 'path';
import psql from '../../common/db/psql';
import loadQuery from '../../common/utils/load-query';
import catchAsync from '../../common/utils/catch-async';
import handlePsqlError from '../../common/utils/handle-pg-error';
import handleSuccessfulJsonResponse from '../../common/utils/handle-succesful-json-response';

import { ReqResNextCallback } from '../../common/types';
import AppException from '../../common/utils/app-exception';

const pathToQuery = `${__dirname}/queries/`;

const createAnswer: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { questionId, userId, content } = req.body;
	const query = await loadQuery(`${pathToQuery}create-answer.sql`);

	psql.query(query, [questionId, userId, content], async (err, results) => {
		if (err) return handlePsqlError(err, next);
		if (!results.rows[0]) return next(new AppException({}, 'Data just bad.'));

		const { id: answerId } = results.rows[0];

		const notifyUsersQuery = await loadQuery(path.join(__dirname, '/../users/queries/notify-users.sql'));
		await psql.query(notifyUsersQuery, [res.locals.user.id]);

		const replyDataQuery = await loadQuery(path.join(`${pathToQuery}/get-questioner-responder-data.sql`));
		const { rows: replyData } = await psql.query(replyDataQuery, [questionId, userId]);

		/* Don't send the notification if someone answers their own question */
		if (userId !== replyData[0].questionerId) {
			res.locals.io.emit('FORUM.NOTIFY.REPLY', {
				message: 'Someone replied to your answer.',
				payload: {
					answerId: answerId,
					questionId,
					questionerId: replyData[0].questionerId,
					questionerEmail: replyData[0].questionerEmail,
					responderId: replyData[0].responderId,
					responderEmail: replyData[0].responderEmail,
					navigateTo: '/my-questions#answer-' + answerId,
				},
			});
			console.log(userId, replyData[0].questionerId, {
				answerId: answerId,
				questionId,
				questionerId: replyData[0].questionerId,
				questionerEmail: replyData[0].questionerEmail,
				responderId: replyData[0].responderId,
				responderEmail: replyData[0].responderEmail,
				navigateTo: '/my-questions#' + answerId,
			});
		}

		handleSuccessfulJsonResponse(res, 201, { answer: results.rows && results.rows[0] }, 'Answer created!');
	});
});

const updateAnswer: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const { content, rating } = req.body;
	const query = await loadQuery(`${pathToQuery}update-answer.sql`);

	psql.query(query, [id, content, rating], (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { answer: results.rows && results.rows[0] }, 'Answer updated!');
	});
});

const deleteAnswer: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const query = await loadQuery(`${pathToQuery}delete-answer.sql`);

	psql.query(query, [id], err => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { answer: {} }, 'Answer deleted!');
	});
});

export default {
	createAnswer,
	updateAnswer,
	deleteAnswer,
};
