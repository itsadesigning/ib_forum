import { Router } from 'express';
import Controllers from './controller';

import protect from '../../middlewares/authenticate';

const router = Router();

router
	.post('/', protect, Controllers.createAnswer)
	.put('/:id', protect, Controllers.updateAnswer)
	.delete('/:id', protect, Controllers.deleteAnswer);

export default router;
