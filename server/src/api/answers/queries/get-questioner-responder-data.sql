SELECT
  "fu"."id" AS "responderId",
  "fu"."email" AS "responderEmail",
  (
    SELECT
      "fq"."userId"
    FROM 
      forum_question fq
    WHERE
      "fq"."id" = $1
  ) AS "questionerId",
  (
    SELECT
      "fu"."email"
    FROM 
      forum_user fu
    WHERE
      "fu"."id" = (
        SELECT
          "fq"."userId"
        FROM 
          forum_question fq
        WHERE
          "fq"."id" = $1
      )
  ) AS "questionerEmail"
FROM 
  forum_user fu
WHERE
  "fu"."id" = $2;