INSERT INTO 
  forum_answer (
    "questionId",
    "userId",
    "content"
  ) 
VALUES 
  ($1, $2, $3)
RETURNING *;