UPDATE
  forum_answer fa
SET 
  "content" = $2,
  "rating" = $3,
  "updatedAt" = CURRENT_TIMESTAMP
WHERE
  "id" = $1
RETURNING 
  "fa"."id",
  "fa"."content",
  "fa"."updatedAt";