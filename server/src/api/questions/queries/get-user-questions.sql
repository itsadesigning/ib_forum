SELECT 
  "fq"."id",
  "fq"."content",
  "fq"."createdAt",
  "fq"."updatedAt",
  "fu"."id" AS "userId",
  "fu"."email",
  "fu"."firstName",
  "fu"."lastName"
FROM
  forum_question fq
LEFT JOIN
  forum_user fu
ON
  "fq"."userId" = "fu"."id"
WHERE 
  "fq"."userId" = $1
ORDER BY 
  "fq"."createdAt" ASC;
