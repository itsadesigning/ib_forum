SELECT 
  "fq"."id",
  "fq"."content",
  "fq"."createdAt",
  "fq"."updatedAt",
  (
    (SELECT DISTINCT
      COUNT(*)
    FROM
      forum_question_rating fqr
    WHERE
      "fqr"."questionId" = "fq"."id"
    AND 
      "fqr"."value" = true)
    -
    (SELECT DISTINCT
      COUNT(*)
    FROM
      forum_question_rating fqr
    WHERE
      "fqr"."questionId" = "fq"."id"
    AND 
      "fqr"."value" = false)
  ) as "rating",
  "fu"."id" AS "userId",
  "fu"."email",
  "fu"."firstName",
  "fu"."lastName"
FROM
  forum_question fq
LEFT JOIN
  forum_user fu
ON
  "fq"."userId" = "fu"."id"
ORDER BY "fq"."createdAt" DESC;