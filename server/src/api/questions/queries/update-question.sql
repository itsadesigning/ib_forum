UPDATE
  forum_question fq
SET 
  "content" = $2,
  "rating" = $3,
  "updatedAt" = CURRENT_TIMESTAMP
WHERE
  "id" = $1
RETURNING 
  "fq"."id",
  "fq"."content",
  "fq"."createdAt",
  "fq"."updatedAt";