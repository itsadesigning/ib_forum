SELECT
 "fa"."id",
 "fa"."userId",
 "fa"."questionId",
 "fa"."content",
 "fa"."rating",
 "fa"."createdAt",
 "fa"."updatedAt",
  (
    (SELECT DISTINCT
      COUNT(*)
    FROM
      forum_answer_rating far
    WHERE
      "far"."answerId" = "fa"."id"
    AND 
      "far"."value" = true)
    -
    (SELECT DISTINCT
      COUNT(*)
    FROM
      forum_answer_rating far
    WHERE
      "far"."answerId" = "fa"."id"
    AND 
      "far"."value" = false)
  ) as "rating",
 "fu"."email"
FROM 
  forum_answer fa
LEFT JOIN
  forum_user fu
ON
  "fa"."userId" = "fu"."id"
WHERE
  "questionId" = $1
ORDER BY "createdAt" ASC;