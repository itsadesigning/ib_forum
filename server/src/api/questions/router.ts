import { Router } from 'express';
import Controllers from './controller';

import protect from '../../middlewares/authenticate';
import pagination from '../../middlewares/pagination';

const router = Router();

router
	.get('/', pagination, Controllers.getQuestions)
	.get('/:id', Controllers.getQuestion)
	.post('/', protect, Controllers.createQuestion)
	.put('/:id', protect, Controllers.updateQuestion)
	.delete('/:id', protect, Controllers.deleteQuestion);

export default router;
