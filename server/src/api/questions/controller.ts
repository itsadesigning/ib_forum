import psql from '../../common/db/psql';
import loadQuery from '../../common/utils/load-query';
import catchAsync from '../../common/utils/catch-async';
import handlePsqlError from '../../common/utils/handle-pg-error';
import handleSuccessfulJsonResponse from '../../common/utils/handle-succesful-json-response';

import { ReqResNextCallback } from '../../common/types';
import AppException from '../../common/utils/app-exception';
import { filterByTypes } from '../../common/utils/filter-questions';

const pathToQuery = `${__dirname}/queries/`;

const getQuestions: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { userId, answers, filterTypes } = req.query;

	const queryCallback = async (err, results) => {
		if (err) return handlePsqlError(err, next);
		if (!results.rows[0]) return next(new AppException({}, 'There are no questions asked yet!'));
		const questions = results.rows;

		if (answers) {
			const query = await loadQuery(`${pathToQuery}get-answers-for-question.sql`);
			for (const [i, question] of questions.entries()) {
				const { rows } = await psql.query(query, [question.id]);
				questions[i].answers = rows;
			}
		}

		const filteredResponse = filterTypes && (await filterByTypes(JSON.parse(filterTypes), questions));

		handleSuccessfulJsonResponse(res, 200, { questions, filteredResponse });
	};

	let query;
	if (userId) {
		query = await loadQuery(`${pathToQuery}get-user-questions.sql`);
		psql.query(query, [userId], queryCallback);
	} else {
		query = await loadQuery(`${pathToQuery}get-all-questions.sql`);
		psql.query(query, queryCallback);
	}
});

const getQuestion: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const query = await loadQuery(`${pathToQuery}get-question.sql`);

	psql.query(query, [id], (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { question: results.rows && results.rows[0] });
	});
});

const createQuestion: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { userId, content } = req.body;
	const query = await loadQuery(`${pathToQuery}create-question.sql`);

	psql.query(query, [userId, content], (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { question: results.rows && results.rows[0] }, 'Question created!');
	});
});

const updateQuestion: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const { content, rating } = req.body;
	const query = await loadQuery(`${pathToQuery}update-question.sql`);

	psql.query(query, [id, content, rating], (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { question: results.rows && results.rows[0] }, 'Question updated!');
	});
});

const deleteQuestion: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const query = await loadQuery(`${pathToQuery}delete-question.sql`);

	psql.query(query, [id], err => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { question: {} }, 'Question deleted!');
	});
});

export default {
	getQuestions,
	getQuestion,
	createQuestion,
	updateQuestion,
	deleteQuestion,
};
