UPDATE
  forum_answer_rating
SET 
  "value" = $3
WHERE
  "answerId" = $1 
AND 
  "userId" = $2
RETURNING *;