UPDATE
  forum_question_rating
SET 
  "value" = $3
WHERE
  "questionId" = $1 
AND 
  "userId" = $2
RETURNING *;