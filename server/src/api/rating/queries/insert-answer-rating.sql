INSERT INTO
  forum_answer_rating(
    "answerId", 
    "userId", 
    "value"
  )
VALUES 
  ($1, $2, $3)
RETURNING *;