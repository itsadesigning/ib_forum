INSERT INTO
  forum_question_rating(
    "questionId", 
    "userId", 
    "value"
  )
VALUES 
  ($1, $2, $3)
RETURNING *;