import { Router } from 'express';
import Controllers from './controller';

import protect from '../../middlewares/authenticate';

const router = Router();

router
	.post('/', protect, Controllers.insertRating)
	.put('/', protect, Controllers.updateRating)
	.delete('/', protect, Controllers.deleteRating);

export default router;
