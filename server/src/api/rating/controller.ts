import psql from '../../common/db/psql';
import loadQuery from '../../common/utils/load-query';
import catchAsync from '../../common/utils/catch-async';
import handlePsqlError from '../../common/utils/handle-pg-error';
import handleSuccessfulJsonResponse from '../../common/utils/handle-succesful-json-response';

import { ReqResNextCallback } from '../../common/types';
import AppException from '../../common/utils/app-exception';

const pathToQuery = `${__dirname}/queries/`;

const insertRating: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { userId, value } = req.body;
	const { questionId, answerId } = req.query;

	const queryCallback = (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { rating: results.rows && results.rows[0] }, 'Rating added!');
	};

	console.log('insert', questionId, userId, value);

	if (questionId) {
		const query = await loadQuery(`${pathToQuery}insert-question-rating.sql`);
		psql.query(query, [questionId, userId, value], queryCallback);
	} else if (answerId) {
		const query = await loadQuery(`${pathToQuery}insert-answer-rating.sql`);
		psql.query(query, [answerId, userId, value], queryCallback);
	} else {
		next(new AppException({}, 'Missing questionId or answerId query params.'));
	}
});

const updateRating: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { userId, value } = req.body;
	const { questionId, answerId } = req.query;

	const queryCallback = (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { rating: results.rows && results.rows[0] }, 'Rating updated!');
	};

	console.log('update', questionId, userId, value);

	if (questionId) {
		const query = await loadQuery(`${pathToQuery}update-question-rating.sql`);
		psql.query(query, [questionId, userId, value], queryCallback);
	} else if (answerId) {
		const query = await loadQuery(`${pathToQuery}update-answer-rating.sql`);
		psql.query(query, [answerId, userId, value], queryCallback);
	} else {
		next(new AppException({}, 'Missing questionId or answerId query params.'));
	}
});

const deleteRating: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { questionId, answerId, userId } = req.query;

	const queryCallback = (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 201, { rating: results.rows && results.rows[0] }, 'Rating removed!');
	};

	console.log(questionId, userId);
	if (questionId) {
		const query = await loadQuery(`${pathToQuery}delete-question-rating.sql`);
		psql.query(query, [questionId, userId], queryCallback);
	} else if (answerId) {
		const query = await loadQuery(`${pathToQuery}delete-answer-rating.sql`);
		psql.query(query, [answerId, userId], queryCallback);
	} else {
		next(new AppException({}, 'Missing questionId or answerId query params.'));
	}
});

export default {
	insertRating,
	updateRating,
	deleteRating,
};
