import { Router } from 'express';
import Controllers from './controller';

import protect from '../../middlewares/authenticate';

const router = Router();

router
	.get('/', Controllers.getUsers)
	.get('/:id', Controllers.getUser)
	.put('/:id', protect, Controllers.updateUser)
	.delete('/:id', protect, Controllers.deleteUser);

export default router;
