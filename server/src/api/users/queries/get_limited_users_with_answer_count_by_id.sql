SELECT 
  "id",
  "email",
  "firstName",
  "lastName",
  (
    SELECT 
      COUNT(*) 
    FROM 
      forum_answer fa
    WHERE
      "fu"."id" = "fa"."userId" 
  ) AS "answersCount"
FROM 
  forum_user fu
LIMIT $1;