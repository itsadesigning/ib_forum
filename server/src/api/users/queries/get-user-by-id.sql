SELECT 
  "id",
  "email",
  "firstName",
  "lastName",
  "createdAt",
  "updatedAt",
  "verified"
FROM 
  forum_user
WHERE
  "id" = $1;