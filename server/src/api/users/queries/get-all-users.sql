SELECT 
  "id",
  "email",
  "firstName",
  "lastName",
  "createdAt",
  "updatedAt",
  "password",
  "verificationCode",
  "verified"
FROM 
  forum_user
ORDER BY 
  "id" ASC;
