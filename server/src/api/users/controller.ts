import bcrypt from 'bcrypt';
import psql from '../../common/db/psql';
import loadQuery from '../../common/utils/load-query';
import catchAsync from '../../common/utils/catch-async';
import handlePsqlError from '../../common/utils/handle-pg-error';
import generateInsertUserQuery from '../../common/utils/generate-insert-user-query';
import { ReqResNextCallback } from '../../common/types';
import AppException from '../../common/utils/app-exception';
import handleSuccessfulJsonResponse from '../../common/utils/handle-succesful-json-response';
import jwt from '../../common/config/jwt';

const pathToQuery = `${__dirname}/queries/`;

const getUsers: ReqResNextCallback = catchAsync(async (_, res, next) => {
	const query = await loadQuery(`${pathToQuery}get-all-users.sql`);

	psql.query(query, (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { users: results.rows && results.rows });
	});
});

const getUser: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const query = await loadQuery(`${pathToQuery}get-user-by-id.sql`);

	psql.query(query, [id], (err, results) => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { user: results.rows && results.rows[0] });
	});
});

const updateUser: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);

	if (req.body.password) {
		req.body.password = await bcrypt.hash(req.body.password, 10);
	}

	const query = generateInsertUserQuery({ id, ...req.body });

	psql.query(query, async (err, results) => {
		if (err) return handlePsqlError(err, next);
		const user = results.rows[0];
		if (!user) next(new AppException({}, 'User does not exist!'));
		const accessToken = await jwt.updateToken({ result: user }, res.locals.accessToken);

		handleSuccessfulJsonResponse(res, 200, { user: results.rows && results.rows[0], accessToken: accessToken });
	});
});

const deleteUser: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const id = parseInt(req.params.id, 10);
	const query = await loadQuery(`${pathToQuery}delete-user.sql`);

	psql.query(query, [id], err => {
		if (err) return handlePsqlError(err, next);
		handleSuccessfulJsonResponse(res, 200, { user: {} }, 'User deleted!');
	});
});

export default {
	getUsers,
	getUser,
	updateUser,
	deleteUser,
};
