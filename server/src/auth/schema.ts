import Joi, { string } from 'joi';

export const registerSchema = Joi.object({
	email: Joi.string().email().min(3).max(30).required(),
	firstName: Joi.string().required(),
	lastName: Joi.string().required(),
	password: Joi.string()
		.pattern(/^[a-zA-Z0-9]{5,18}$/)
		.required(),
	repeatPassword: Joi.ref('password'),
});

export const loginSchema = Joi.object({
	email: Joi.string().email().min(3).max(30).required(),
	password: Joi.string()
		.pattern(/^[a-zA-Z0-9]{5,18}$/)
		.required(),
});

export const verifySchema = Joi.object({
	email: Joi.string().email().required(),
	verificationCode: string().required(),
});
