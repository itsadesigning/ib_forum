SELECT 
  "id",
  "email",
  "firstName",
  "lastName",
  "createdAt",
  "updatedAt",
  "password",
  "verificationCode",
  "notification",
  "verified"
FROM 
  forum_user
WHERE
  "email" = $1;