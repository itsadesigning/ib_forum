UPDATE
  forum_user
SET
  "verified" = true
WHERE 
  "email" = $1
RETURNING
  "id",
  "email",
  "firstName",
  "lastName",
  "createdAt",
  "updatedAt",
  "verified";