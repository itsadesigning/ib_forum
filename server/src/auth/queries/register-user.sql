INSERT INTO 
  forum_user (
    "firstName",
    "lastName",
    "email",
    "password",
    "verificationCode"
  ) 
VALUES 
  ($1, $2, $3, $4, $5)
RETURNING *;
