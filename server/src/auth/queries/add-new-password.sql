UPDATE
  forum_user
SET
  "password" = $2,
  "updatedAt" = NOW()
WHERE
  "email" = $1
RETURNING
  "email";
  