import { Router } from 'express';
import Controllers from './controller';
import protect from '../middlewares/authenticate';

const router = Router();

router
	.post('/login', Controllers.login)
	.post('/login-check', protect, Controllers.loginCheck)
	.post('/logout', protect, Controllers.logout)
	.post('/resend-verification-code', protect, Controllers.resendVerificationCode)
	.post('/reset-password', Controllers.resetPassword)
	.post('/register', Controllers.register, Controllers.login)
	.get('/verify', Controllers.verify);

export default router;
