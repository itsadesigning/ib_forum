import bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';
import cryptoRandomString from 'crypto-random-string';

import psql from '../common/db/psql';
import loadQuery from '../common/utils/load-query';
import catchAsync from '../common/utils/catch-async';
import handlePsqlError from '../common/utils/handle-pg-error';
import { loginSchema, registerSchema, verifySchema } from './schema';
import { ReqResNextCallback } from '../common/types';
import AppException from '../common/utils/app-exception';
import { USER_NOT_FOUND } from '../common/utils/exception-codes';
import jwt from '../common/config/jwt';
import Sendgrid from '../common/config/sendgrid';
import handleSuccessfulJsonResponse from '../common/utils/handle-succesful-json-response';
import Joi from 'joi';

const pathToQuery = `${__dirname}/queries/`;

const login: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;

	await loginSchema.validateAsync({ email, password });
	const query = await loadQuery(`${pathToQuery}get-user-by-email.sql`);

	psql.query(query, [email], async (err, results) => {
		if (err) return handlePsqlError(err, next);
		if (!results.rows[0]) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));

		const loggedInUser = results.rows[0];
		const isMatch = await bcrypt.compare(password, loggedInUser.password);
		if (!isMatch) {
			return next(new AppException({}, 'Incorrect email and/or password!'));
		}

		const user = {
			id: loggedInUser.id,
			email: loggedInUser.email,
			firstName: loggedInUser.firstName,
			lastName: loggedInUser.lastName,
			createdAt: loggedInUser.createdAt,
			updatedAt: loggedInUser.updatedAt,
			verified: loggedInUser.verified,
			notification: loggedInUser.notification,
		};

		const accessToken = await jwt.sign({ result: user });

		handleSuccessfulJsonResponse(res, 200, { accessToken, user }, 'Successfull login!');
	});
});

/**
 * Goes through protect middleware first where it sets token and user to request object
 */

const loginCheck: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const email = res.locals.user.email;
	if (!email) return next(new AppException({}, 'There is no user logged in with provided token!'));

	const query = await loadQuery(`${pathToQuery}get-user-by-email.sql`);

	psql.query(query, [email], (err, results) => {
		if (err) return handlePsqlError(err, next);
		if (!results.rows[0]) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));

		const { accessToken, user = results.rows[0] } = res.locals;

		handleSuccessfulJsonResponse(res, 200, { accessToken, user }, 'User is logged in!');
	});
});

/**
 * Goes throught protect middleware to check if token has expired
 */

const logout: ReqResNextCallback = catchAsync(async (req, res, next) => {
	if (!res.locals.user.email) return next(new AppException({}, 'There is no user logged in with provided token!'));

	jwt.blacklist(res.locals.accessToken);

	handleSuccessfulJsonResponse(res, 200, { accessToken: null, user: null }, 'User is logged out!');
});

const register: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { firstName, lastName, email, password } = req.body;

	await registerSchema.validateAsync({ firstName, lastName, email, password });
	const hashedPassword = await bcrypt.hash(password, 10);
	const query = await loadQuery(`${pathToQuery}register-user.sql`);
	const uuid = uuidv4();

	psql.query(query, [firstName, lastName, email, hashedPassword, uuid], async (err, results) => {
		if (err) return handlePsqlError(err, next);
		if (!results.rows[0]) return next(new AppException({}, 'Registration went wrong! No user.'));

		await Sendgrid.sendVerificationMessage(email, uuid, results.rows[0]);

		/* Logins user right after registration to create and attach an accessToken */
		next();
	});
});

const verify: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { email, verificationCode } = req.query;

	await verifySchema.validateAsync({ email, verificationCode });
	const query = await loadQuery(`${pathToQuery}verify-user.sql`);

	psql.query(query, [email], async (err, results) => {
		if (err) return handlePsqlError(err, next);
		const user = results.rows[0];

		if (!user) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));
		res.locals.user = user;
		res.locals.message = 'User successfully verified!';

		res.locals.accessToken = await jwt.updateToken({ result: res.locals.user });

		await res.locals.io.emit('FORUM.UPDATE_VERIFY', {
			payload: { accessToken: res.locals.accessToken, user },
			message: 'Token verified!',
		});

		res.set('Content-Type', 'text/html');
		res.status(200).send(Buffer.from('<h2>Test String</h2>'));
	});
});

const resendVerificationCode: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { email } = req.body;

	const query = await loadQuery(`${pathToQuery}get-verification-code.sql`);

	psql.query(query, [email], async (err, results) => {
		if (err) return handlePsqlError(err, next);

		const user = results.rows[0];

		if (!user) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));
		const { verificationCode: uuid, verified } = results.rows[0];

		if (verified) return next(new AppException({}, 'User is already verified'));

		await Sendgrid.sendVerificationMessage(email, uuid, verified);

		handleSuccessfulJsonResponse(res, 200, {}, 'Verification mail sent!');
	});
});

const resetPassword: ReqResNextCallback = catchAsync(async (req, res, next) => {
	const { email } = req.body;

	await Joi.string().email().validate(email);
	const query = await loadQuery(`${pathToQuery}get-user-by-email.sql`);

	psql.query(query, [email], async (err, results) => {
		if (err) return handlePsqlError(err, next);

		const user = results.rows[0];
		if (!user) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));

		const newRandomPassword = cryptoRandomString({ length: 10 });
		const hashedPassword = await bcrypt.hash(newRandomPassword, 10);

		const query = await loadQuery(`${pathToQuery}add-new-password.sql`);

		psql.query(query, [email, hashedPassword], async (err, results) => {
			if (err) return handlePsqlError(err, next);

			const user = results.rows[0];
			if (!user) return next(new AppException(USER_NOT_FOUND, USER_NOT_FOUND.message(email)));

			await Sendgrid.sendPasswordResetMessage(email, newRandomPassword);

			handleSuccessfulJsonResponse(res, 200, {}, 'New password sent to email!');
		});
	});
});

export default {
	login,
	loginCheck,
	logout,
	register,
	verify,
	resendVerificationCode,
	resetPassword,
};
