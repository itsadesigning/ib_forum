import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';

import Socket from './common/config/socket';
import Routers from './routers';
import errorHandler from './middlewares/error-handler';
import env from './common/config/env';

class Server {
	private app: express.Application;
	private server;
	public socket;

	constructor() {
		this.app = express();
		this.setMiddlewares();
	}

	private setSocketToRequest(req, res, next) {
		res.locals.io = this.socket;
		next();
	}

	private setMiddlewares() {
		this.app.use(cors({ origin: env.allowedOrigins, credentials: true }));
		this.app.use(morgan('dev'));
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(bodyParser.json({ limit: '1mb' }));
		this.app.use(helmet());
	}

	private setRouters() {
		this.app.use(this.setSocketToRequest.bind(this));
		this.app.use('/', express.static(__dirname + '/../static'));
		this.app.use('/auth', Routers.auth);
		this.app.use('/api/users', Routers.users);
		this.app.use('/api/questions', Routers.questions);
		this.app.use('/api/answers', Routers.answers);
		this.app.use('/api/rating', Routers.rating);
		this.app.all('*', Routers.notFound);
		this.app.use(errorHandler);
	}

	public start = (port: number): Promise<number> => {
		return new Promise((res, rej) => {
			this.server = this.app.listen(port, () => res(port));
			this.server.on('error', err => rej(err));
			this.socket = new Socket(this.server);
			this.setRouters();
		});
	};
}

export default Server;
