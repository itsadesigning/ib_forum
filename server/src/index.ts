import Funnies from 'funnies';

import Server from './server';
import env from './common/config/env';

const PORT = parseInt(env.port || '8001', 10);
const funnies = new Funnies();

const init = async () => {
	try {
		const app = new Server();
		await app.start(PORT); // I can see it being used later.
		console.log(funnies.message(), ` on port ${PORT}...`);
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
};

init();

process.on('unhandledRejection', function (reason, p) {
	console.error('Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
});
