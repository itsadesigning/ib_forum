import { Request, Response, NextFunction } from 'express';
import AppError from '../utils/app-exception';

export type ReqResNextCallback = (req?: Request, res?: Response, next?: NextFunction) => void;

export type ErrCallback = (err: AppError, req: Request, res: Response, next?: NextFunction) => void;

export type UserType = {
	id?: number;
	firstName?: string;
	lastName?: string;
	email?: string;
	notification?: boolean;
	createdAt?: Date | string;
	updatedAt?: Date | string;
};

export type UsersType = UserType[];

export type QuestionType = {
	id?: number;
	userId?: number;
	content?: string;
	rating?: number;
	createdAt?: Date | string;
	updatedAt?: Date | string;
	answers: [];
};

export type QuestionsType = QuestionType[];

export type AnswerType = {
	id?: number;
	questionId?: number;
	userId?: number;
	content?: string;
	rating?: number;
	createdAt?: Date | string;
	updatedAt?: Date | string;
};

export type AnswersType = AnswerType[];

export type Pagination = {
	size?: number;
	page?: number;
	offset?: number;
	limit?: number;
};
