import { Pool } from 'pg';

import settings from '../config/settings';

const psql = new Pool(settings.psql);

export default psql;
