const generateInsertUserQuery = data => {
	const filteredEntries = Object.entries(data).filter(
		([key]) => ['firstName', 'lastName', 'password'].indexOf(key) > -1,
	);

	const query = `
    UPDATE
      forum_user
    SET
			"updatedAt" = NOW(),
			${filteredEntries.map(([key, value]) => `"${key}" = '${value}'`)}
    WHERE
			"id" = ${data.id}
		RETURNING
			"id",
			"email",
			"firstName",
			"lastName",
			"createdAt",
			"updatedAt",
			"verified"
		`;

	return query;
};

export default generateInsertUserQuery;
