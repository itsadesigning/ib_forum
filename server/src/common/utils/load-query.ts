import fs from 'fs';

export const loadQuery = async (pathToQuery: string): Promise<string> => {
	return new Promise<string>(res => {
		fs.readFile(pathToQuery, (err, query) => {
			res(query.toString());
		});
	});
};

export default loadQuery;
