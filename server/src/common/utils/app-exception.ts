import { __prod__ } from '../constants';
import * as exceptionCodes from './exception-codes';

class AppException extends Error {
	public code: number | string;
	public httpCode: number;
	public stack;
	public status;

	constructor(public error: any, message?: string, public details: any[] = []) {
		super(message);

		this.setHttpCode(error);
		this.setCode(error);
		this.setMessage(error, message);
		this.setDetails(error);
		this.setStack(message);

		Error.captureStackTrace(this, this.constructor);
	}

	setHttpCode(exception): void {
		if (exception && exception.httpCode) {
			this.httpCode = exception.httpCode;
		} else if (exception && exception.statusCode) {
			this.httpCode = exception.statusCode;
		} else {
			this.httpCode = exceptionCodes.THIRD_PARTY_ERROR.httpCode;
		}
	}

	setCode(exception): void {
		if (!this.code && exception && exception.code) {
			this.code = exception.code;
		} else {
			this.code = exceptionCodes.UNKNOWN_ERROR.code;
		}
	}

	setMessage(exception, message): void {
		if (message) {
			this.message = message;
		} else {
			this.message = (exception && exception.message) || 'Something went wrong!';
		}
	}

	setDetails(exception) {
		if (exception.details && Array.isArray(exception.details)) {
			exception.details.forEach(detail => this.details.push(detail));
		}
	}

	setStack(message): void {
		if (typeof Error.captureStackTrace === 'function') {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = new Error(message).stack;
		}

		// Add stack to details on development environment
		if (Array.isArray(this.details) && !__prod__) {
			this.details.push({
				stack: this.stack,
			});
		}
	}

	static unhandledError(response): AppException {
		return new AppException(exceptionCodes.UNKNOWN_ERROR.code, response.code);
	}
}

export default AppException;
