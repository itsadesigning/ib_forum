import AppException from './app-exception';

const catchAsync = fn => (req, res, next): Promise<void> => {
	return fn(req, res, next).catch(err => next(new AppException(err)));
};

export default catchAsync;
