/* eslint-disable */

/* Standard HTTP errors */

/* To be updated and used mandatorly, too much work for now */

/* Check http-status-codes on npm */

export const UNKNOWN_ERROR = {
	code: 'UNKNOWN_ERROR',
	httpCode: 400,
	error: 'unknown_error',
	message: 'Unknown error.',
};

export const UNAUTHORIZED = {
	code: 'UNAUTHORIZED',
	httpCode: 401,
	error: 'unauthorized',
	message: 'You are unauthorized!',
};

export const FORBIDDEN = {
	code: 'FORBIDDEN',
	httpCode: 403,
	error: 'forbidden',
	message: 'This resource is forbidden.',
};

export const SOMETHING_WENT_WRONG = {
	code: 'SOMETHING_WENT_WRONG',
	httpCode: 404,
	error: 'not_found',
	message: 'Something went terribly wrong!',
};

export const NOT_FOUND = {
	code: 'NOT_FOUND',
	httpCode: 404,
	error: 'not_found',
	message: $1 => `Can't find ${$1} on this server!`,
};

export const USER_NOT_FOUND = {
	code: 'USER_NOT_FOUND',
	httpCode: 401,
	error: 'unauthorized',
	message: $1 => `User with an email ${$1} is not registered!`,
};

export const THIRD_PARTY_ERROR = {
	code: 'THIRD_PARTY_ERROR',
	httpCode: 418,
	error: 'third_party_error',
	message: 'Third party error.',
};

export const MISSING_PARAMS = {
	code: 'MISSING_PARAMS',
	httpCode: 400,
	error: 'missing_params',
	message: "You're missing some required params.",
};

export const WRONG_PARAMS = {
	code: 'WRONG_PARAMS',
	httpCode: 400,
	error: 'wrong_params',
	message: "You've sent wrong params.",
};

export const WRONG_QUERY_PARAMS = {
	code: 'WRONG_QUERY_PARAMS',
	httpCode: 400,
	error: 'wrong_query_params',
	message: "You've sent wrong query params.",
};

export const ER_DUP_ENTRY = {
	code: 'DUPLICATE_DB_RESULT',
	httpCode: 409,
	error: 'duplicate_db_result',
	message: 'File already available in the database.',
};

export const MISSING_HEADERS = {
	code: 'MISSING_HEADERS',
	httpCode: 400,
	error: 'nonexistent_file',
	message: 'Missing appropriate headers.',
};
