import path from 'path';
import psql from '../db/psql';
import { QuestionsType, Pagination, AnswerType, UsersType } from '../types';
import loadQuery from './load-query';

const FILTER_TYPES = {
	latestQuestions(questions: QuestionsType, { limit }: Pagination): QuestionsType {
		return questions
			.sort((a, b) => {
				return new Date(a.createdAt).getMilliseconds() - new Date(b.createdAt).getMilliseconds();
			})
			.slice(0, limit);
	},
	async usersWithMostAnswers(questions: QuestionsType, { limit }: Pagination): Promise<UsersType> {
		const usersAnswerCount = {};
		questions.forEach(question => {
			question.answers.forEach((answer: AnswerType) => {
				let x = usersAnswerCount[answer.userId];
				usersAnswerCount[answer.userId] = x > -1 ? x++ : 1;
			});
		});

		const pathToQuery = path.join(__dirname, '/../../api/users/queries/get_limited_users_with_answer_count_by_id.sql');
		const query = await loadQuery(pathToQuery);
		const { rows } = await psql.query(query, [limit]);

		return rows.sort((x, y) => y.answersCount - x.answersCount);
	},
	questionsWithMostLikes(questions: QuestionsType, { limit }: Pagination): QuestionsType {
		return questions.sort((x, y) => y.rating - x.rating).slice(0, limit);
	},
};

export const filterByTypes = async (filterTypes, questions) => {
	const filteredResponse = {};
	for (const [filterType, params] of Object.entries(filterTypes)) {
		if (FILTER_TYPES[filterType]) {
			filteredResponse[filterType] = await FILTER_TYPES[filterType](questions, params);
		}
	}

	return filteredResponse;
};
