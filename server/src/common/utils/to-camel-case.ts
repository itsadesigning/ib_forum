const toCamelCase = (str: string): string =>
	str.replace(/([-_][a-z])/g, group => group.toUpperCase().replace('-', '').replace('_', ''));

export default toCamelCase;
