import { Response } from 'express';

type HandleSuccessfulJsonResponse = (res: Response, statusCode: number, data: unknown, message?: string) => void;

const handleSuccessfulJsonResponse: HandleSuccessfulJsonResponse = (res, statusCode, data, message = 'Success!') => {
	res.status(statusCode).json({
		status: 'success',
		data,
		message,
	});
};

export default handleSuccessfulJsonResponse;
