import AppException from './app-exception';
import psqlErrorCodes from '../db/psqlErrorCodes';

const handlePgError = (err, next, message?): void => {
	const psqlErrCode = psqlErrorCodes[err.code] && psqlErrorCodes[err.code].code;
	const psqlErrMessage = psqlErrorCodes[err.code] && psqlErrorCodes[err.code].message;
	next(new AppException({ psqlErrCode, psqlErrMessage }, message || psqlErrMessage, [{ ...err }]));
};

export default handlePgError;
