import sg from '@sendgrid/mail';

import env from './env';

const mailHtmlTemplates = {
	verify: (to, uuid, verified) => {
		const verificationUrl = `${env.apiUrl}/auth/verify?verificationCode=${uuid}&email=${to}`;
		return `
			<h3>Verify email</h3>
			<br />
			<hr />
			<h4>Verification code: ${uuid}</h4>
			<h4 style="font-weight: 400;">Click here to verify: <a href="${verificationUrl}&verified=${verified}">${verificationUrl}</a></h4>
		`;
	},
	resetPassword: password => `
    <h4>Reset password</h4>
    <br />
    <hr />
    <h5>New password: ${password}</h5>
  `,
};

class Sendgrid {
	constructor(private apiKey = env.sendgridApiKey) {
		sg.setApiKey(apiKey);
	}

	async sendVerificationMessage(to, uuid, verified) {
		await sg.send({
			to: to,
			from: env.sendgridMailFrom,
			subject: 'Verification mail from the Forum',
			html: mailHtmlTemplates.verify(to, uuid, verified),
		});
	}

	async sendPasswordResetMessage(to, newPassword) {
		await sg.send({
			to: to,
			from: env.sendgridMailFrom,
			subject: 'Password reset mail from the Forum',
			html: mailHtmlTemplates.resetPassword(newPassword),
		});
	}
}

const instance = new Sendgrid();

export default instance;
