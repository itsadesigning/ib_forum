import env from './env';

export default {
	psql: {
		max: 10,
		user: env.psqlDbUser,
		host: env.psqlDbHost,
		database: env.psqlDbName,
		password: env.psqlDbPassword,
		port: env.psqlDbPort,
	},
};
