import io from 'socket.io';
import { Http2Server } from 'http2';
import AppException from '../utils/app-exception';

/**
 * Client events:
 * FORUM.HEALTH_CHECKS
 *
 * Server events:
 * FORUM.UPDATE_VERIFY
 * FORUM.NOTIFY
 */

class Socket {
	private server;
	public socket;
	constructor(server: Http2Server) {
		this.server = server;
		this.socket = io(server);
	}

	/* Saw this, added for fun sakes */
	checkConnectionsCount(): void {
		this.server.getConnections((err, count) => {
			if (err) {
				console.error(err);
				process.exit(1);
			} else {
				console.log(count);
			}
		});
	}

	emit(event: string, data: unknown): Promise<void> {
		return new Promise((resolve, reject) => {
			if (!this.socket) return reject('No socket connection.');

			this.socket.emit(event, data);
		});
	}

	on(event: string, func: () => unknown): void | AppException {
		if (!this.socket) return new AppException({}, 'No socket connection available.');

		this.socket.on(event, func);
	}
}

export default Socket;
