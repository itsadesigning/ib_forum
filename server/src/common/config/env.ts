import dotenv from 'dotenv-flow';
import path from 'path';
import { __prod__ } from '../constants';

dotenv.config({
	path: path.join(__dirname + `/../../../env`),
	silent: true || __prod__,
});

const env = {
	environment: process.env.NODE_ENV,
	port: process.env.PORT,
	apiUrl: process.env.API_URL,
	allowedOrigins: process.env.ALLOWED_ORIGINS,
	psqlDbUser: process.env.POSTGRES_USER,
	psqlDbHost: process.env.POSTGRES_HOST,
	psqlDbName: process.env.POSTGRES_DB,
	psqlDbPassword: process.env.POSTGRES_PASSWORD,
	psqlDbPort: process.env.POSTGRES_PORT,
	psqlDbUrl: process.env.POSTGRES_URL,
	jwtPrivate: process.env.JWT_PRIVATE,
	jwtPublic: process.env.JWT_PUBLIC,
	jwtExpire: process.env.JWT_EXPIRE,
	sendgridApiKey: process.env.SENDGRID_API_KEY,
	sendgridMailFrom: process.env.SENDGRID_MAIL_FROM,
};

export default env;
