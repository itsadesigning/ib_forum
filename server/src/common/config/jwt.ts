import jwt from 'jsonwebtoken';
import env from '../config/env';
import AppException from '../utils/app-exception';

class TokenGenerator {
	private blacklistedTokens: string[];

	constructor(
		public secretOrPrivateKey = env.jwtPrivate,
		public secretOrPublicKey = env.jwtPublic,
		public options = {},
	) {
		this.secretOrPrivateKey = secretOrPrivateKey;
		this.secretOrPublicKey = secretOrPublicKey;
		this.options = options;
		/* Transfer to db (?) */
		this.blacklistedTokens = [];
	}

	blacklist(token: string): void {
		this.blacklistedTokens.push(token);
	}

	checkIsBlacklisted(token: string): boolean {
		return this.blacklistedTokens.some(blacklistedToken => token === blacklistedToken);
	}

	trimAuthHeaders(authHeader) {
		if (authHeader && authHeader.startsWith('Bearer')) return authHeader.split(' ')[1];
	}

	async sign(payload, signOptions?): Promise<string> {
		const jwtSignOptions = Object.assign({}, signOptions, this.options);
		return await jwt.sign(payload, this.secretOrPrivateKey, jwtSignOptions);
	}

	async updateToken(payload, token?, signOptions?) {
		this.blacklist(token);
		return await this.sign(payload, signOptions);
	}

	async verify(token): Promise<any> {
		if (this.checkIsBlacklisted(token)) throw new AppException({}, 'Token is no longer in use', [{ token }]);
		return await jwt.verify(token, this.secretOrPrivateKey);
	}

	async refresh(token, refreshOptions?): Promise<string> {
		const payload = await jwt.verify(token, this.secretOrPublicKey, refreshOptions.verify);
		delete payload.iat;
		delete payload.exp;
		delete payload.nbf;
		delete payload.jti;
		const jwtSignOptions = Object.assign({}, this.options, { jwtid: refreshOptions.jwtid });
		return await jwt.sign(payload, this.secretOrPrivateKey, jwtSignOptions);
	}
}

const instance = new TokenGenerator();

export default instance;
