import React, { useEffect, useState, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Typography, Collapse, Table, Button, Divider } from "antd";
import { I18nContext } from "../../i18n/index";
import { getQuestions } from "../../store/actions/question";

import styles from "./Home.module.scss";

const genColumn = (title, dataIndex) => ({ title, dataIndex });

const Home = () => {
  const { translate } = useContext(I18nContext);
  const [filterParams, setFilterParams] = useState({
    latestQuestions: {
      limit: 20,
    },
    usersWithMostAnswers: {
      limit: 20,
    },
    questionsWithMostLikes: {
      limit: 20,
    },
  });
  const dispatch = useDispatch();
  const filteredResponse = useSelector(
    ({ question }) => question.filteredResponse
  );

  useEffect(() => {
    dispatch(
      getQuestions({
        answers: true,
        filterTypes: { ...filterParams },
      })
    );
  }, [dispatch, filterParams]);

  const loadMore = (filterType) =>
    setFilterParams({
      ...filterParams,
      [filterType]: {
        limit: filterParams[filterType].limit + 20,
      },
    });

  return (
    <Collapse defaultActiveKey={["1"]} ghost>
      {[
        {
          title: translate('home.collapse.latest_questions'),
          columns: [
            genColumn(translate('table.column.question'), "content"),
            genColumn(translate('table.column.user_email'), "email"),
          ],
          filterType: "latestQuestions",
        },
        {
          title: translate('home.collapse.most_answers_by_user'),
          columns: [
            genColumn(translate('table.column.user_email'), "email"),
            genColumn(translate('table.column.answers_count'), "answersCount"),
          ],
          filterType: "usersWithMostAnswers",
        },
        {
          title: translate("home.collapse.hot_questions"),
          columns: [
            genColumn(translate('table.column.question'), "content"),
            genColumn(translate('table.column.user_email'), "email"),
            genColumn(translate('table.column.likes_count'), "rating"),
          ],
          filterType: "questionsWithMostLikes",
        },
      ].map(({ title, columns, filterType }, i) => (
        <>
          <Collapse.Panel
            header={<Typography.Title level={2}>{title}</Typography.Title>}
            key={i}
          >
            <Table
              columns={columns}
              dataSource={filteredResponse[filterType]}
              pagination={false}
              bordered
            />
            <Button
              block
              type="dashed"
              shape="round"
              size="large"
              className={styles["load-more-button"]}
              disabled={
                filteredResponse[filterType] &&
                filteredResponse[filterType].length !==
                  filterParams[filterType].limit
              }
              onClick={() => loadMore(filterType)}
            >
              {translate('table.load_more')}
            </Button>
          </Collapse.Panel>
          <Divider dashed style={{ color: "#ddd" }} />
        </>
      ))}
    </Collapse>
  );
};

export default Home;
