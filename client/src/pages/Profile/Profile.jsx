import React, { useState, useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Form,
  Input,
  Button,
  Typography,
  PageHeader,
  Divider,
  Modal,
} from "antd";
import {
  CheckCircleTwoTone,
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { I18nContext } from '../../i18n/index';
import { deleteUser, editUser } from "../../store/actions/user";
import { resendVerificationCode } from "../../store/actions/auth";

import { pageHeader } from "../index.module.scss";
import styles from "./Profile.module.scss";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 16 },
};

const wrapperCol = { offset: 4, span: 16 };

const Profile = () => {
  const { translate } = useContext(I18nContext);
  const { id, firstName, lastName, email, verified } = useSelector(
    ({ auth }) => auth.loggedUser
  );
  const notification = useSelector(({ app }) => app.notification);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  let oldFieldValues;

  useEffect(() => {
    oldFieldValues = { email, firstName, form, lastName };
    form.setFieldsValue(oldFieldValues);
  }, [email, firstName, form, lastName]);

  useEffect(() => {
    setModalLoading(false);
  }, [notification]);

  const onFinish = ({ firstName, lastName, password }) => {
    dispatch(
      editUser({
        id,
        user: {
          firstName,
          lastName,
          password,
        },
      })
    );
  };

  const onResendVerificationCode = () =>
    dispatch(resendVerificationCode({ email }));

  const handleDeleteModalOk = () => {
    setModalLoading(true);
    dispatch(deleteUser({ id }));
  };

  const handleDeleteModalCancel = () => {
    setModalVisible(false);
  };

  const onValuesChange = () => {
    const fieldValues = form.getFieldsValue();
  };

  return (
    <>
      <PageHeader className={pageHeader} title={translate('page.title.profile')} />
      <Divider dashed style={{ color: "#ccc" }}></Divider>
      <Form
        {...layout}
        form={form}
        onFinish={onFinish}
        onValuesChange={onValuesChange}
        size="large"
      >
        <Form.Item label={translate('form.label.first_name')} name="firstName">
          <Input />
        </Form.Item>
        <Form.Item label={translate('form.label.last_name')} name="lastName">
          <Input />
        </Form.Item>
        <Form.Item label={translate('form.label.email')} name="email">
          <Input disabled />
        </Form.Item>
        <Form.Item
          label={translate('form.label.password')}
          name="password"
          rules={[
            { min: 5, message: "Password must be minimum 5 characters." },
            { max: 18, message: "Password must be maximum 18 characters." },
            {
              pattern: /^[a-zA-Z0-9]*$/,
              message: "Password must contain only alphanumeric values!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label={translate('form.label.repeat_password')}
          name="repeat-password"
          dependencies={["password"]}
          rules={[
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (getFieldValue("password") && !value) {
                  return Promise.reject(translate('form.validation_msg.confirm_password'));
                }
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  translate('form.validation_msg.no_match_password')
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item label={translate('form.label.verified')}>
          {verified ? (
            <CheckCircleTwoTone style={{ fontSize: "20px" }} />
          ) : (
            <>
              <CloseCircleOutlined style={{ fontSize: "20px" }} />
            </>
          )}
        </Form.Item>
        <Form.Item
          wrapperCol={wrapperCol}
          onClick={onResendVerificationCode}
          hidden={verified}
        >
          <Typography.Text className={styles["resend-verification-code"]}>
            {translate('profile.resend_verification_code')}
          </Typography.Text>
        </Form.Item>
        <Form.Item wrapperCol={wrapperCol}>
          <Button
            type="primary"
            shape="round"
            size="large"
            htmlType="submit"
            style={{ marginRight: "15px" }}
          >
            {translate('btn.edit')}
          </Button>
          <Button
            type="danger"
            shape="round"
            size="large"
            onClick={() => setModalVisible(true)}
          >
            {translate('btn.delete')}
          </Button>
        </Form.Item>
      </Form>
      <Modal
        visible={modalVisible}
        title={translate('profile.delete_user_warning_title')}
        icon={<ExclamationCircleOutlined />}
        okText={translate('btn.delete')}
        okType="danger"
        cancelText={translate('btn.no')}
        onOk={handleDeleteModalOk}
        onCancel={handleDeleteModalCancel}
        confirmLoading={modalLoading}
      >
        {translate('profile.delete_user_warning_content')}
      </Modal>
    </>
  );
};

export default Profile;
