import React, { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Typography, Divider } from "antd";

import { I18nContext} from '../../i18n/index';
import Question from "../../components/Question/Question";
import CommentEditor from "../../components/CommentEditor/CommentEditor";
import { getQuestions, createQuestion } from "../../store/actions/question";

const Questions = () => {
  const { translate } = useContext(I18nContext);
  const { isAuthenticated, loggedUser } = useSelector(({ auth }) => auth);
  const questions = useSelector(({ question }) => question.questions);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getQuestions({ answers: true }));
  }, [dispatch, isAuthenticated]);

  const onCreateNewQuestion = ({ content }) => {
    content &&
      isAuthenticated &&
      dispatch(createQuestion({ userId: loggedUser.id, content }));
  };

  return (
    <>
      <Typography.Title level={2}>{translate('page.title.questions')}</Typography.Title>
      {isAuthenticated && (
        <>
          <Divider orientation="left" dashed style={{ color: "#ccc" }}>
          {translate('questions.subheader.create_new')}
          </Divider>
          <CommentEditor type="create" onSubmit={onCreateNewQuestion} />
        </>
      )}
      <Divider orientation="left" dashed style={{ color: "#ccc" }}>
      {translate('questions.subheader.questions_list')}
      </Divider>
      {questions.map((question) => (
        <Question {...question}></Question>
      ))}
    </>
  );
};

export default Questions;
