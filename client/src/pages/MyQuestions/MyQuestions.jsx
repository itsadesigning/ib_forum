import React, { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Divider, Typography } from "antd";
import { I18nContext } from '../../i18n/index';
import Question from "../../components/Question/Question";
import { getQuestions } from "../../store/actions/question";
import { removeBadgeNotification } from "../../store/actions/auth";

const MyQuestions = () => {
  const { translate } = useContext(I18nContext);
  const dispatch = useDispatch();
  const { isAuthenticated, loggedUser } = useSelector(({ auth }) => auth);
  const { questions } = useSelector(({ question }) => question);

  useEffect(() => {
    dispatch(removeBadgeNotification());
    dispatch(getQuestions({ answers: true, userId: loggedUser.id }));
  }, [dispatch, isAuthenticated, loggedUser.id]);

  return (
    <>
      <Typography.Title level={2}>{translate('page.title.my_questions')}</Typography.Title>
      <Divider dashed style={{ color: "#ccc" }}></Divider>
      {questions.map((question) => (
        <Question id={question.id} key={question.id} {...question}></Question>
      ))}
    </>
  );
};

export default MyQuestions;
