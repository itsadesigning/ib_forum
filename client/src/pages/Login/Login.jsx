import React, { useState, useEffect, useContext } from "react";
import { Link, withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Button, Checkbox, Typography, Spin } from "antd";
/* Allgedly, tree-shaking is applied from this import, though Webpack does it itself. Still gonna keep it way */
import isEmail from "validator/es/lib/isEmail";
import { I18nContext } from "../../i18n/index";
import { login, resetPassword } from "../../store/actions/auth";

import styles from "./Login.module.scss";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 10 },
};

const wrapperCol = { offset: 8, span: 12 };

const Login = ({ history }) => {
  const { translate} = useContext(I18nContext);
  const [form] = Form.useForm();
  const [pending, setPending] = useState(false);
  const [submitDisabled, setSubmitDisabled] = useState(true);
  const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);
  const { notification } = useSelector(({ app }) => app);
  const dispatch = useDispatch();

  useEffect(() => {
    setPending(false);
    isAuthenticated && history.push("/");
  }, [form, history, isAuthenticated]);

  useEffect(() => {
    setPending(false);
  }, [notification]);

  const onFormSubmit = ({ email, password }) => {
    setPending(true);
    dispatch(login({ email, password }));
  };

  const onResetPassword = async () => {
    const { email } = await form.validateFields(["email"]);
    if (!email || !isEmail(email)) return;
    setPending(true);
    dispatch(resetPassword({ email }));
  };

  const checkDisabled = async () => {
    form.validateFields(["email", "password"]).catch(({ errorFields }) => {
      setSubmitDisabled(errorFields.length);
    });
  };

  return (
    <>
      <Spin size="large" spinning={pending}>
        <Form
          label={translate('form.label.register')}
          name="register"
          size="large"
          form={form}
          onValuesChange={checkDisabled}
          onFinish={onFormSubmit}
          {...layout}
        >
          <Form.Item wrapperCol={wrapperCol}>
            <Typography.Title level={2}>Sign in</Typography.Title>
          </Form.Item>
          <Form.Item
            label={translate('form.label.email')}
            name="email"
            rules={[
              { required: true, message: translate('form.validation_msg.required_email') },
              { type: "email", message: translate('form.validation_msg.type_email') },
              { min: 3, message: translate('form.validation_msg.min_3_chars_email') },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={translate('form.label.password')}
            name="password"
            extra={
              <Typography.Text className="ant-form-text">
                {translate('login.forgot_password')}{" "}
                <Typography.Text
                  className={styles["reset-password"]}
                  onClick={onResetPassword}
                >
                  {translate('login.reset_now')}
                </Typography.Text>
              </Typography.Text>
            }
            rules={[
              {
                min: 5,
                required: true,
                message: translate('form.validation_msg.min_5_chars_password'),
              },
              { max: 18, message: translate('form.validation_msg.max_18_chars_password') },
              {
                pattern: /^[a-zA-Z0-9]*$/,
                message: translate('form.validation_msg.alphanum_password'),
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label={translate('form.label.remember_password')}
            name="rememberPassword"
            valuePropName="checked"
          >
            <Checkbox />
          </Form.Item>
          <Form.Item wrapperCol={wrapperCol}>
            <Typography.Text className="ant-form-text" type="secondary">
              {translate('login.no_account')} {<Link to="/register">{translate('login.register')}</Link>}
            </Typography.Text>
          </Form.Item>
          <Form.Item wrapperCol={wrapperCol}>
            <Button type="primary" htmlType="submit" disabled={submitDisabled}>
              {translate('btn.sign_in')}
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </>
  );
};

export default withRouter(Login);
