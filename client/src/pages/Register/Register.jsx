import React, { useState, useEffect, useContext } from "react";
import { withRouter, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Button, Checkbox, Typography, Spin } from "antd";

import { I18nContext} from '../../i18n/index';
import { register } from "../../store/actions/auth";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 10 },
};

const wrapperCol = { offset: 8, span: 16 };

const Register = ({ history }) => {
  const { translate } = useContext(I18nContext)
  const [submitDisabled, setSubmitDisabled] = useState(true);
  const [pending, setPending] = useState(false);
  const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);
  const notification = useSelector(({ app }) => app.notification);
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  useEffect(() => {
    setPending(false);
    isAuthenticated && history.push("/");
  }, [history, isAuthenticated]);

  useEffect(() => {
    setPending(false);
  }, [notification]);

  const onFormSubmit = (values) => {
    setPending(true);
    dispatch(register(values));
  };

  const checkDisabled = async () => {
    form
      .validateFields(["email", "password", "repeatPassword", "terms"])
      .catch(({ errorFields }) => {
        setSubmitDisabled(errorFields.length);
      });
  };

  return (
    <Spin size="large" spinning={pending}>
      <Form
        label={translate('form.label.register')}
        name="register"
        size="large"
        form={form}
        onFinish={onFormSubmit}
        onValuesChange={checkDisabled}
        {...layout}
      >
        <Form.Item wrapperCol={wrapperCol}>
          <Typography.Title level={2}>{translate('page.title.register')}</Typography.Title>
        </Form.Item>
        <Form.Item
          label={translate('form.label.first_name')}
          name="firstName"
          rules={[{ required: true, message: translate('form.validation_msg.required_first_name') }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={translate('form.label.last_name')}
          name="lastName"
          rules={[{ required: true, message: translate('form.validation_msg.required_last_name') }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={translate('form.label.email')}
          name="email"
          rules={[
            { type: "email", message: translate('form.validation_msg.type_email') },
            { min: 3, message: translate('form.validation_msg.min_3_chars_email') },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={translate('form.label.password')}
          name="password"
          rules={[
            {
              required: true,
              message: translate('form.validation_msg.required_password'),
            },
            { min: 5, message: translate('form.validation_msg.min_3_chars_email')},
            { max: 18, message: translate('form.validation_msg.min_5_chars_password') },
            {
              pattern: /^[a-zA-Z0-9]*$/,
              message: translate('form.validation_msg.alphanum_password'),
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label={translate('form.label.repeat_password')}
          name="repeat-password"
          dependencies={["password"]}
          rules={[
            {
              required: true,
              message: translate('form.validation_msg.confirm_password'),
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve(translate('form.validation_msg.confirm_password'));
                }
                return Promise.reject(
                  translate('form.validation_msg.no_match_password')
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label={translate('form.label.terms')}
          name="terms"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value
                  ? Promise.resolve()
                  : Promise.reject(translate('form.validation_msg.terms')),
            },
          ]}
        >
          <Checkbox />
        </Form.Item>
        <Form.Item label>
          <Typography.Text className="ant-form-text" type="secondary">
            {translate('register.already_have_account')} {<Link to="/login">{translate('register.login')}</Link>}
          </Typography.Text>
        </Form.Item>
        <Form.Item wrapperCol={wrapperCol}>
          <Button
            type="primary"
            htmlType="submit"
            shape="round"
            size="large"
            disabled={submitDisabled}
          >
            {translate('btn.submit')}
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

export default withRouter(Register);
