import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { Typography } from "antd";

import { getUser } from "../../store/actions/user";

const User = ({
  match: {
    params: { id },
  },
}) => {
  const visitedUser = useSelector(({ user }) => user.visitedUser);
  console.log(visitedUser);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser(id));
  }, [id, dispatch]);

  return (
    <>
      <Typography.Title level={2}>User</Typography.Title>
      <Typography.Title level={3}>
        First name: {visitedUser.firstName}
      </Typography.Title>
      <Typography.Title level={3}>
        Last name: {visitedUser.lastName}
      </Typography.Title>
      <Typography.Title level={3}>Email: {visitedUser.email}</Typography.Title>
      <Typography.Title level={3}>
        Created at: {visitedUser.createdAt}
      </Typography.Title>
      <Typography.Title level={3}>
        Verified: {visitedUser.verified ? "Y" : "N"}
      </Typography.Title>
    </>
  );
};

export default withRouter(User);
