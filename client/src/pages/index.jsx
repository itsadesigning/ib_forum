import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { notification as Notification } from "antd";
/* Local imports */
import AppLayout from "../components/AppLayout/AppLayout";
import routes from "../routes";
import AuthRoute from "../hoc/AuthRoute";
import { loginCheck, removeLoggedUser } from "../store/actions/auth";
import { useSocket } from "../hooks/useSocket";
/* Style imports */
import "../styles/index.scss";

const App = () => {
  const {
    isAuthenticated,
    accessToken,
    initialLoginChecked,
    loggedUser,
  } = useSelector(({ auth }) => auth);
  const notification = useSelector(({ app }) => app.notification);
  const dispatch = useDispatch();
  const [connectSocket, disconnectSocket] = useSocket([
    "FORUM.UPDATE_VERIFY",
    "FORUM.NOTIFY.REPLY",
  ]);

  useEffect(() => {
    connectSocket();
    return () => disconnectSocket();
  }, [connectSocket, disconnectSocket, dispatch]);

  /* Check whether token still exits. If it does, check its validity, if it doesn't force logout, and clear storage */
  useEffect(() => {
    if (!accessToken || accessToken === "undefined") {
      localStorage.removeItem("accessToken");
      dispatch(removeLoggedUser());
    } else if (!initialLoginChecked) {
      dispatch(loginCheck({ accessToken }));
    }
  }, [accessToken, initialLoginChecked, dispatch, loggedUser.email]);

  /* App wide notification system. Connected to Redux */
  useEffect(() => {
    if (!Object.keys(notification).length) return;
    const { status, message, options } = notification;
    console.log(options && options.description);
    Notification[status]({
      message,
      description: options && options.description,
      placement: "bottomRight",
      duration: 3,
    });
  }, [notification]);

  const renderRoute = ({ path, component, exact, name, requiresAuth }) => {
    return React.createElement(requiresAuth ? AuthRoute : Route, {
      key: name,
      path,
      component,
      exact,
      name,
      isAuthenticated,
      requiresAuth,
    });
  };

  return (
    <Router>
      <AppLayout>
        <Route
          render={({ location }) => (
            <TransitionGroup>
              <CSSTransition
                key={location.key}
                enter={true}
                exit={false}
                timeout={500}
                classNames="fade"
              >
                <Switch location={location}>{routes.map(renderRoute)}</Switch>
              </CSSTransition>
            </TransitionGroup>
          )}
        ></Route>
      </AppLayout>
    </Router>
  );
};

export default App;
