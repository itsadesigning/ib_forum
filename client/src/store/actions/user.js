import userApi from "../../api/user";
import handleServerResponse from "../../utils/handleServerResponse";
import { createNotification } from "./app";

import { SET_USERS, SET_VISITED_USER } from "../types";
import { setLoggedUser, removeLoggedUser } from "../actions/auth";

/* Action creators */

export const setUsers = (users) => ({
	type: SET_USERS,
	users: users,
});

export const setVisitedUser = (data) => ({
	type: SET_VISITED_USER,
	user: data.user,
});

/* Async actions */

export const getUser = (id) => async (dispatch) => {
	const [err, data] = await handleServerResponse(userApi.getUser(id));
	if (err) return;
	dispatch(setVisitedUser(data));
};

export const editUser = ({ id, user }) => async (dispatch) => {
	const [err, data, [status, message]] = await handleServerResponse(
		userApi.editUser({ id, user })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(setLoggedUser(data));
};

export const deleteUser = ({ id }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		userApi.deleteUser({ id })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(removeLoggedUser());
};
