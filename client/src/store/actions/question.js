import questionApi from "../../api/question";
import handleServerResponse from "../../utils/handleServerResponse";
import { createNotification } from "./app";

import { SET_QUESTIONS } from "../types";

export const setQuestions = ({ questions = [], filteredResponse }) => ({
	type: SET_QUESTIONS,
	questions,
	filteredResponse,
});

export const getQuestions = (params) => async (dispatch) => {
	const [err, data] = await handleServerResponse(
		questionApi.getQuestions(params)
	);
	if (err || !data) return;
	dispatch(setQuestions(data));
};

export const createQuestion = ({ userId, content }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.createQuestion({ userId, content })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const editQuestion = ({ id, content }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.editQuestion({ id, content })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const deleteQuestion = ({ id }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.deleteQuestion({ id })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const createAnswer = ({ questionId, userId, content }) => async (
	dispatch
) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.createAnswer({ questionId, userId, content })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const editAnswer = ({ id, content }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.editAnswer({ id, content })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const deleteAnswer = ({ id }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.deleteAnswer({ id })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const addRating = (data, params) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.addRating(data, params)
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const updateRating = (data, params) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.updateRating(data, params)
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};

export const removeRating = (params) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		questionApi.removeRating(params)
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(getQuestions({ answers: true }));
};
