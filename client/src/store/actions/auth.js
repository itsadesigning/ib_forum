import AuthAPI from "../../api/auth";
import {
	SET_LOGGED_USER,
	SET_BADGE_NOTIFICATION,
	REMOVE_BADGE_NOTIFICATION,
	LOG_OUT,
} from "../types";
import handleServerResponse from "../../utils/handleServerResponse";

import { createNotification } from "../actions/app";

/**
 * AuthAPI ( and the others ) capitalized because of readability
 */

export const setLoggedUser = (data) => {
	localStorage.setItem("accessToken", data.accessToken);

	return {
		type: SET_LOGGED_USER,
		accessToken: data.accessToken,
		user: data.user,
	};
};

export const setBadgeNotification = () => ({
	type: SET_BADGE_NOTIFICATION,
});

export const removeBadgeNotification = () => ({
	type: REMOVE_BADGE_NOTIFICATION,
});

export const removeLoggedUser = () => {
	localStorage.removeItem("accessToken");

	return {
		type: LOG_OUT,
	};
};

export const register = (user) => async (dispatch) => {
	const [err, data, [status, message]] = await handleServerResponse(
		AuthAPI.register(user)
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(setLoggedUser(data));
};

export const login = (loginInfo) => async (dispatch) => {
	const [err, data, [status, message]] = await handleServerResponse(
		AuthAPI.login(loginInfo)
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(setLoggedUser(data));
};

export const loginCheck = ({ accessToken }) => async (dispatch) => {
	const [err, data] = await handleServerResponse(
		AuthAPI.loginCheck({ accessToken })
	);
	if (err) {
		dispatch(removeLoggedUser());
	}
	dispatch(setLoggedUser({ initialLoginChecked: true, ...data }));
};

export const logout = ({ accessToken }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		AuthAPI.logout({ accessToken })
	);
	dispatch(createNotification(status, message));
	if (err) return;
	dispatch(removeLoggedUser());
};

export const resendVerificationCode = ({ email }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		AuthAPI.resendVerificationCode({ email })
	);
	dispatch(createNotification(status, message));
	if (err) return;
};

export const resetPassword = ({ email }) => async (dispatch) => {
	const [err, , [status, message]] = await handleServerResponse(
		AuthAPI.resetPassword({ email })
	);
	dispatch(createNotification(status, message));
	if (err) return;
};
