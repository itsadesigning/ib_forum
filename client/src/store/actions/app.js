import { CREATE_NOTIFICATION, REMOVE_NOTIFICATION } from "../types";

export const createNotification = (status, message, options) => ({
	type: CREATE_NOTIFICATION,
	message,
	status,
	options,
});

export const removeNotification = () => ({
	type: REMOVE_NOTIFICATION,
});
