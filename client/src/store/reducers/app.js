import { CREATE_NOTIFICATION, REMOVE_NOTIFICATION } from "../types";

export const initialState = {
	notification: {},
	requestPending: false,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CREATE_NOTIFICATION:
			return {
				...state,
				notification: {
					message: action.message,
					status: action.status,
					description: action.description,
					options: action.options,
				},
			};
		case REMOVE_NOTIFICATION:
			return {
				...state,
				notification: {},
			};
		default:
			return state;
	}
};

export default reducer;
