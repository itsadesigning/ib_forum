import { SET_QUESTIONS } from "../types";

export const initialState = {
	questions: [],
	filteredResponse: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_QUESTIONS:
			return {
				...state,
				questions: action.questions,
				filteredResponse: action.filteredResponse || state.filteredResponse,
			};
		default:
			return state;
	}
};

export default reducer;
