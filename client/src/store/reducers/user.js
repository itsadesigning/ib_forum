import { SET_USERS, SET_VISITED_USER } from "../types";

export const initialState = {
	users: [],
	visitedUser: {},
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_USERS:
			return {
				...state,
				users: action.users,
			};
		case SET_VISITED_USER:
			return {
				...state,
				visitedUser: action.user,
			};
		default:
			return state;
	}
};

export default reducer;
