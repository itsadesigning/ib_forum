import { combineReducers } from "redux";
import appReducer from "./app";
import authReducer from "./auth";
import userReducer from "./user";
import questionReducer from "./question";

const rootReducer = combineReducers({
	app: appReducer,
	auth: authReducer,
	user: userReducer,
	question: questionReducer,
});

export default rootReducer;
