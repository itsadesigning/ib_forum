import {
	SET_LOGGED_USER,
	LOG_OUT,
	SET_BADGE_NOTIFICATION,
	REMOVE_BADGE_NOTIFICATION,
} from "../types";

const localStoredAccessToken = localStorage.getItem("accessToken");

const initialState = {
	loggedUser: {
		badgeNotification: false,
	},
	initialLoginChecked: false,
	isAuthenticated:
		localStoredAccessToken !== "undefined" && !!localStoredAccessToken,
	accessToken: localStorage.getItem("accessToken"),
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_LOGGED_USER:
			return {
				...state,
				accessToken: action.accessToken,
				loggedUser: {
					...state.loggedUser,
					...action.user,
				},
				isAuthenticated: true,
				initialLoginChecked: true,
			};
		case SET_BADGE_NOTIFICATION:
			return {
				...state,
				loggedUser: {
					...state.loggedUser,
					badgeNotification: true,
				},
			};
		case REMOVE_BADGE_NOTIFICATION:
			return {
				...state,
				loggedUser: {
					...state.loggedUser,
					badgeNotification: false,
				},
			};
		case LOG_OUT:
			return {
				...state,
				accessToken: null,
				loggedUser: {},
				isAuthenticated: false,
			};
		default:
			return state;
	}
};

export default reducer;
