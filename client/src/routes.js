import Home from "./pages/Home/Home";
import Questions from "./pages/Ouestions/Questions";
import MyQuestions from "./pages/MyQuestions/MyQuestions";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Profile from "./pages/Profile/Profile";
import User from "./pages/User/User";

const routes = [
	{
		name: "Home",
		path: "/",
		component: Home,
		exact: true,
	},
	{
		name: "Questions",
		path: "/questions",
		component: Questions,
	},
	{
		name: "MyQuestions",
		path: "/my-questions",
		component: MyQuestions,
		requiresAuth: true,
	},
	{
		name: "User",
		path: "/user/:id",
		component: User,
		hide: true,
	},
	{
		name: "Login",
		path: "/login",
		component: Login,
		hideIfAuthenticated: true,
	},
	{
		name: "Register",
		path: "/register",
		component: Register,
		hideIfAuthenticated: true,
	},
	{
		name: "Profile",
		path: "/profile",
		component: Profile,
		requiresAuth: true,
	},
];

export default routes;
