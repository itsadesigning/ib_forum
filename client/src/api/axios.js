import axios from "axios";

const instance = axios.create({
	baseURL: process.env.REACT_APP_API_BASE_URL,
});

instance.interceptors.request.use((config) => {
	const token = localStorage.getItem("accessToken");

	if (!token) return config;

	config.headers.Authorization = `Bearer ${token}`;

	return config;
});

instance.interceptors.response.use(
	(res) => res.data,
	(err) => Promise.reject(err.response && err.response.data)
);

export default instance;
