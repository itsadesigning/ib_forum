import axios from "./axios";

const getQuestions = (params) => {
	return axios.get("/api/questions", {
		params,
	});
};

const createQuestion = ({ userId, content }) => {
	return axios.post("/api/questions", { userId, content });
};

const editQuestion = ({ id, content }) => {
	return axios.put(`/api/questions/${id}`, content);
};

const deleteQuestion = ({ id }) => {
	return axios.delete(`/api/questions/${id}`);
};

/**
 * Creates new answer
 * @param {Object} data
 * @param {number} data.userId
 * @param {number} data.id
 * @param {string} data.answer
 */

const createAnswer = ({ questionId, userId, content }) => {
	return axios.post("/api/answers", { questionId, userId, content });
};

const editAnswer = ({ id, content }) => {
	return axios.put(`/api/answers/${id}`, { content });
};

const deleteAnswer = ({ id }) => {
	return axios.delete(`/api/answers/${id}`);
};

const addRating = (data, params) => {
	return axios.post("/api/rating", data, {
		params,
	});
};

const updateRating = (data, params) => {
	return axios.put("/api/rating", data, {
		params,
	});
};

const removeRating = (params) => {
	return axios.delete("/api/rating", {
		params,
	});
};

export default {
	getQuestions,
	createQuestion,
	editQuestion,
	deleteQuestion,
	createAnswer,
	editAnswer,
	deleteAnswer,
	addRating,
	updateRating,
	removeRating,
};
