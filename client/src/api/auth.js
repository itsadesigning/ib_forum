/* Auth stuff */

import axios from "./axios";

const register = (data) => {
	return axios.post("/auth/register", data);
};

const login = (data) => {
	return axios.post("/auth/login", data);
};

const loginCheck = (data) => {
	return axios.post("/auth/login-check", data);
};

const logout = (data) => {
	return axios.post("/auth/logout", data);
};

const resendVerificationCode = ({ email }) => {
	return axios.post("/auth/resend-verification-code", { email });
};

const resetPassword = ({ email }) => {
	return axios.post("/auth/reset-password", { email });
};

export default {
	register,
	login,
	loginCheck,
	logout,
	resendVerificationCode,
	resetPassword,
};
