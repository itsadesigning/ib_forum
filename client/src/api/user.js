import axios from "./axios";

/* 
	? Can be generalized to GET, POST, PUT, DELETE methods 
	? But what prevents from using axiod directly?
	? Universal { data: , params: } parameter sent to all api calls?
*/

const getUsers = () => {
	return axios.get("/api/users");
};

const getUser = (id) => {
	return axios.get(`/api/users/${id}`);
};

const editUser = ({ id, user }) => {
	return axios.put(`/api/users/${id}`, user);
};

const deleteUser = ({ id }) => {
	return axios.delete(`/api/users/${id}`);
};

export default {
	getUsers,
	getUser,
	editUser,
	deleteUser,
};
