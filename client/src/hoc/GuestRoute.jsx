import React from "react";
import { Redirect, Route } from "react-router-dom";

const GuestRoute = ({ isAuthenticated, ...props }) => {
  if (!isAuthenticated) {
    return <Route {...props} />;
  } else {
    return <Redirect to="/" />;
  }
};

export default GuestRoute;
