import React, { useEffect } from "react";
import { Redirect, Route, useHistory } from "react-router-dom";

const AuthRoute = ({ isAuthenticated, ...props }) => {
  if (!isAuthenticated) {
    return <Redirect to="/login" />;
  } else {
    return <Route {...props} />;
  }
};

export default AuthRoute;
