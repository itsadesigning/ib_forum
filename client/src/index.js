import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import App from "./pages";
import * as serviceWorker from "./serviceWorker";

import store from "./store";

import { I18nContextProvider } from "./i18n";

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<I18nContextProvider>
				<App />
			</I18nContextProvider>
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);

serviceWorker.unregister();
