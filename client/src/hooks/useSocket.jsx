import React from "react";
import { useDispatch, useSelector } from "react-redux";
import io from "socket.io-client";
import { createNotification } from "../store/actions/app";
import { setLoggedUser, setBadgeNotification } from "../store/actions/auth";

export const useSocket = (e_types) => {
  const { id } = useSelector(({ auth }) => auth.loggedUser);
  const dispatch = useDispatch();
  const socket = io.connect(process.env.REACT_APP_API_BASE_URL);

  console.log(socket);

  const EVENTS = {
    "FORUM.UPDATE_VERIFY"() {
      return socket.on("FORUM.UPDATE_VERIFY", ({ message, payload }) => {
        dispatch(setLoggedUser(payload));
        dispatch(createNotification("success", message));
      });
    },
    "FORUM.NOTIFY.REPLY"() {
      return socket.on("FORUM.NOTIFY.REPLY", ({ message, payload }) => {
        if (id === payload.responderId) return;
        const options = {
          /* This is just a concept. Though it would be a bit harder to implement properly w/o history or react ref since html id is not rendered */
          description: (
            <span>
              User {payload.responderEmail} replyed to your question. Click{" "}
              {<a href={payload.navigateTo}>here</a>} for more data.
            </span>
          ),
        };
        dispatch(setBadgeNotification());
        dispatch(createNotification("info", message, options));
      });
    },
    "FORUM.NOTIFY.RATING"() {
      console.log("FORUM.NOTIFY.RATING");
      return socket.on("FORUM.NOTIFY.REPLY", ({ message, payload }) => {
        console.log("INNER FORUM.NOTIFY.RATING");
        dispatch(setBadgeNotification());
        dispatch(createNotification("info", message, payload));
      });
    },
  };

  let e_uppers = {};
  let e_downers = {};
  for (let e_type of e_types) {
    e_uppers[e_type] = EVENTS[e_type];
    e_downers[e_type] = socket.off(e_type);
  }

  const connect = () => {
    e_types.forEach((e_type) => e_uppers[e_type]());
    return e_uppers;
  };

  const disconnect = socket.disconnect.bind(socket);

  return [connect, disconnect, e_uppers, e_downers];
};
