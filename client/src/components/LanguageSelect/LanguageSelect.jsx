import React, { useContext, useState } from "react";
import { Select } from "antd";
import { I18nContext } from "../../i18n/index";

const LanguageSelect = (props) => {
  let [selected, setSelected] = useState("en");
  const { langCode, dispatch } = useContext(I18nContext);

  const onLanguageSelect = (code) => {
    dispatch({ type: "SET_LANGUAGE", payload: code });
  };

  const renderOption = (code) => (
    <Select.Option value={code}>{code.toUpperCase()}</Select.Option>
  );

  return (
    <Select
      defaultValue={selected}
      onChange={onLanguageSelect}
      style={{ width: 120 }}
    >
      {renderOption("en")}
      {renderOption("bhs")}
    </Select>
  );
};

export default LanguageSelect;
