import React, { useContext } from "react";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Layout, Menu, Badge, Avatar } from "antd";
import { LogoutOutlined, UserOutlined } from "@ant-design/icons";

import { I18nContext } from "../../i18n/index";
import { logout } from "../../store/actions/auth";
import LanguageSelect from "../LanguageSelect/LanguageSelect";
import styles from "./AppLayout.module.scss";

const { Header, Content, Footer } = Layout;

const AppLayout = ({ children, history }) => {
  const { translate } = useContext(I18nContext);
  const { isAuthenticated, accessToken, loggedUser } = useSelector(
    ({ auth }) => auth
  );
  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(logout({ email: loggedUser.email, accessToken }));
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header className={styles.header}>
        <Menu theme="light" mode="horizontal" defaultSelectedKeys={["2"]}>
          <Menu.Item onClick={() => history.push("/")}>
            {translate("menu.home")}
          </Menu.Item>
          <Menu.Item onClick={() => history.push("/questions")}>
            {translate("menu.questions")}
          </Menu.Item>
          <Menu.Item
            onClick={() => history.push("/my-questions")}
            hidden={!isAuthenticated}
          >
            {translate("menu.my_questions")}
          </Menu.Item>
          <Menu.Item
            onClick={() => history.push("/register")}
            className={styles["menu-item"]}
            hidden={isAuthenticated}
          >
            {translate("menu.register")}
          </Menu.Item>
          <Menu.Item
            onClick={() => history.push("/login")}
            className={styles["menu-item"]}
            hidden={isAuthenticated}
          >
            {translate("menu.login")}
          </Menu.Item>
          {isAuthenticated && (
            <>
              <Menu.Item
                className={styles["menu-item"]}
                onClick={onLogout}
                icon={<LogoutOutlined />}
              >
                {translate("menu.logout")}
              </Menu.Item>
              <LanguageSelect />
              <Menu.Item
                onClick={() => history.push("/profile")}
                className={[styles["menu-item"], styles["menu-item-email"]]}
              >
                <span style={{ marginRight: "15px" }}>
                  <Badge dot={loggedUser.badgeNotification}>
                    <Avatar shape="square" icon={<UserOutlined />} />
                  </Badge>
                </span>
                {loggedUser.email}
              </Menu.Item>
            </>
          )}
        </Menu>
      </Header>
      <Content className={styles.content}>{children}</Content>
      <Footer style={{ textAlign: "center", backgroundColor: "#FFFFFF" }}>
        {translate("footer.content")}
      </Footer>
    </Layout>
  );
};

export default withRouter(AppLayout);
