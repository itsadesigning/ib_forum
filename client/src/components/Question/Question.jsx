import React, { useState, useContext, createElement } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Comment, Tooltip, Card } from "antd";
import moment from "moment";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeTwoTone,
  LikeTwoTone,
  DeleteOutlined,
  FormOutlined,
} from "@ant-design/icons";

/* Component to be renamed to something like Inquiry, or whatever. So this can be generalized */
/* Pass these two actions to this component rather than importing them */
import {
  editQuestion,
  deleteQuestion,
  createAnswer,
  editAnswer,
  deleteAnswer,
  updateRating,
  addRating,
  removeRating,
} from "../../store/actions/question";
import { I18nContext } from "../../i18n/index";
import CommentEditor from "../CommentEditor/CommentEditor";
import styles from "./Question.module.scss";

/* This should be done by creating two components and custom react hook. This component has gotten to big. */

const Question = ({
  id,
  userId,
  content,
  email,
  answers,
  isAnswer,
  createdAt,
  rating,
}) => {
  const { translate } = useContext(I18nContext);
  const [likes, setLikes] = useState(false);
  const [dislikes, setDislikes] = useState(false);
  const [action, setAction] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [replyMode, setReplyMode] = useState(false);

  const { isAuthenticated, loggedUser } = useSelector(({ auth }) => auth);
  const dispatch = useDispatch();

  const like = () => {
    if (!likes) {
      setLikes(true);
      setAction("liked");
      if (dislikes) {
        dispatch(
          updateRating(
            { userId, value: true },
            { [!isAnswer ? "questionId" : "answerId"]: id }
          )
        );
      } else {
        dispatch(
          addRating(
            { userId, value: true },
            { [!isAnswer ? "questionId" : "answerId"]: id }
          )
        );
      }
      setDislikes(false);
    } else {
      setLikes(false);
      setAction(null);
      dispatch(
        removeRating({ [!isAnswer ? "questionId" : "answerId"]: id, userId })
      );
    }
  };

  const dislike = () => {
    if (!dislikes) {
      setDislikes(true);
      setAction("disliked");
      if (likes) {
        dispatch(
          updateRating(
            { userId, value: false },
            { [!isAnswer ? "questionId" : "answerId"]: id }
          )
        );
      } else {
        dispatch(
          addRating(
            { userId, value: false },
            { [!isAnswer ? "questionId" : "answerId"]: id }
          )
        );
      }
      setLikes(false);
    } else {
      setDislikes(false);
      setAction(null);
      dispatch(
        removeRating({ [!isAnswer ? "questionId" : "answerId"]: id, userId })
      );
    }
  };

  const toggleReply = () => {
    setReplyMode(!replyMode);
  };

  const toggleEdit = () => {
    setEditMode(!editMode);
  };

  const onEdit = (content) => {
    dispatch(editQuestion({ id, content }));
    setEditMode(false);
  };

  const onDelete = () => {
    dispatch(deleteQuestion({ id }));
  };

  const onReply = ({ content }) => {
    dispatch(createAnswer({ questionId: id, userId: loggedUser.id, content }));
  };

  const onEditAnswer = ({ content }) => {
    dispatch(editAnswer({ id, content }));
    setEditMode(false);
  };

  const onDeleteAnswer = () => {
    dispatch(deleteAnswer({ id }));
  };

  const actions = [
    <Tooltip key="comment-basic-like" title={translate('general.like')}>
      <span className={styles["rating-count"]}>{rating}</span>
      <span className={!isAuthenticated && styles["comment-basic-disabled"]}>
        <span onClick={like} className={`${styles["comment-basic-like"]}`}>
          {createElement(action === "liked" ? LikeTwoTone : LikeOutlined)}
        </span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title={translate('general.dislike')}>
      <span className={!isAuthenticated && styles["comment-basic-disabled"]}>
        <span
          onClick={dislike}
          className={`${styles["comment-basic-dislike"]}`}
        >
          {createElement(
            action === "disliked" ? DislikeTwoTone : DislikeOutlined
          )}
        </span>
      </span>
    </Tooltip>,
    <span className={!isAuthenticated && styles["comment-basic-disabled"]}>
      <span
        hidden={isAnswer}
        key="comment-basic-reply-to"
        className={`${styles["comment-basic-reply-to"]}`}
        onClick={toggleReply}
      >
        {translate("comment.reply_to")}
      </span>
    </span>,
    <DeleteOutlined
      key="comment-basic-delete"
      hidden={userId !== loggedUser.id}
      onClick={!isAnswer ? onDelete : onDeleteAnswer}
      className={styles["comment-basic-delete"]}
    />,
    <FormOutlined
      key="comment-basic-edit"
      hidden={userId !== loggedUser.id}
      onClick={toggleEdit}
      className={styles["comment-basic-edit"]}
    />,
  ];

  return (
    <div className={[!isAnswer && styles["card-parent"]]}>
      <Card className={styles.card} key={id}>
        {!editMode ? (
          <>
            <Comment
              actions={actions}
              className={styles.comment}
              author={<Link to={`/user/${userId}`}>{email}</Link>}
              content={<p className={styles["comment-content"]}>{content}</p>}
              datetime={
                <Tooltip title={translate("general.created_at")}>
                  <span>{moment(createdAt).format("DD-MM-YYYY, h:mm:ss")}</span>
                </Tooltip>
              }
            >
              {answers &&
                answers.map((answer) => {
                  return (
                    <div id={`answer-${answer.id}`}>
                      <div className={styles.answer}>
                        <Question
                          isAnswer={!!answer.questionId}
                          handleReplyEdit={onEditAnswer}
                          handleReplyDelete={onDeleteAnswer}
                          key={`answer-${answer.id}`}
                          {...answer}
                        />
                      </div>
                    </div>
                  );
                })}
            </Comment>
            {replyMode && (
              <div className={styles.reply}>
                <CommentEditor
                  type="reply"
                  toggle={toggleReply}
                  editMode={editMode}
                  onSubmit={onReply}
                ></CommentEditor>
              </div>
            )}
          </>
        ) : (
          <CommentEditor
            type="edit"
            className={styles.edit}
            initialValue={content}
            toggle={toggleEdit}
            editMode={editMode}
            onSubmit={!isAnswer ? onEdit : onEditAnswer}
          ></CommentEditor>
        )}
      </Card>
    </div>
  );
};

export default Question;
