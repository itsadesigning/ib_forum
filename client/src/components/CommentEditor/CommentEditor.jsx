import React, { useContext } from "react";
import { Comment, Form, Input, Button } from "antd";

import { I18nContext } from "../../i18n/index";

import styles from "./CommentEditor.module.scss";

const EditorActions = ({ toggle, type }) => {
  const { translate } = useContext(I18nContext);
  const elements = {
    create: (
      <Button htmlType="submit" type="primary" shape="round" size="large">
        {translate("btn.add_question")}
      </Button>
    ),
    edit: (
      <>
        <Button
          htmlType="submit"
          type="primary"
          shape="round"
          size="large"
          style={{ marginRight: "15px" }}
        >
          {translate("btn.edit_question")}
        </Button>
        <Button onClick={toggle} shape="round" size="large">
          {translate("btn.cancel")}
        </Button>
      </>
    ),
    reply: (
      <>
        <Button
          htmlType="submit"
          type="primary"
          shape="round"
          size="large"
          style={{ marginRight: "15px" }}
        >
          {translate("btn.reply")}
        </Button>
        <Button onClick={toggle} shape="round" size="large">
          {translate("btn.cancel")}
        </Button>
      </>
    ),
  };

  return <Form.Item style={{ marginBottom: 0 }}>{elements[type]}</Form.Item>;
};

const Editor = ({ onSubmit, initialValue, toggle, type }) => {
  const { translate } = useContext(I18nContext);
  const [form] = Form.useForm();

  return (
    <Form form={form} onFinish={onSubmit}>
      <Form.Item
        name="content"
        className={styles["textarea-border"]}
        initialValue={initialValue}
        rules={[
          {
            required: true,
            message: translate("comment.validation_msg.length"),
          },
        ]}
      >
        <Input.TextArea
          rows={4}
          placeholder={translate("comment.placeholder")}
        />
      </Form.Item>
      {<EditorActions type={type} toggle={toggle} />}
    </Form>
  );
};

const CommentEditor = ({ initialValue, onSubmit, type, toggle }) => {
  return (
    <Comment
      content={
        <Editor
          onSubmit={onSubmit}
          initialValue={initialValue}
          type={type}
          toggle={toggle}
        />
      }
    />
  );
};

export default CommentEditor;
