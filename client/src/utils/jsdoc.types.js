/**
 * @typedef {Object} ServerResponseSuccess
 * @property {number} status
 * @property {string} message
 * @property {[Question]} data
 */

/**
 * @typedef {Object} ServerResponseError
 * @property {number} httpCode
 * @property {string} message
 * @property {Error} error
 * @property {[Object]} details
 */

/**
 * @typedef {function} ServerRequestFunction
 * @param {Object} params
 * @param {Object} data
 */

/* Server payloads: */

/**
 * @typedef {Object} Question
 * @property {number} id
 * @property {number} userId
 * @property {string} question
 * @property {string} createdAt
 * @property {string} updatedAt
 */
