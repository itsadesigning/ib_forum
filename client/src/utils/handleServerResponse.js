const handleServerResponse = (promise) =>
	promise
		.then(({ data, message = "Something went terribly good!" }) => {
			return [null, data, ["success", message]];
		})
		.catch((err) => {
			const message = (err && err.message) || "Something went terribly wrong!";
			return [err, null, ["error", message]];
		});

export default handleServerResponse;
