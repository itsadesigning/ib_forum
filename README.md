### Done

[x] Configure `docker-compose` to run Node (node:12.18-slim), React (node:12.18-slim), postgres(postgres:13.0-alpine), pgAdmin, Swagger (Redis eventually)<br>
[x] Write SQL queries instead of using ORM (practice purpose, though I believe I already see the advantage)<br>
[x] Execute migrations on each `docker-compose up` so the app has some starting data <br>
[x] Successful JWT registration with validation email sent to the user (Sendgrid), and direct redirection to Homepage with `accessToken` saved to `localStorage` (will be updated to sessions or `refreshToken` will be used to improve security) <br>
[x] Reset email sent if user forgets to login <br>
[x] Client (AntD, validator) and server (Joi) side validation for each request. Identical to data definition in SQL queries.<br>
[x] App wide notification system implemented with Redux store <br>
[x] On each page refresh `/login-check` is sent to the server to see if the JWT is still valid<br>
[x] On Profile update new JWT is created, the old one blacklisted and directly stored to `localStorage` w/o disrupting user experience<br>
[x] Button for `Resend verification code` <br>
[x] Allow creation, editing and deleting new questions, answers with rating supported for each <br>
[x] Load answers recursively, using single component for both questions and answers (self-invoking, kinda proud)<br>
[x] On Logout, `localStorage` is cleansed of `accessToken`, which is firstly sent to the server to be blacklisted<br>
[x] Write functional components with react hooks to improve code effiency, remove Component class boilerplate, and incline to functional programming (personal preference, much to learn though)<br>
[x] Create higher order components for Auth and Guest routes <br>
[x] Create a custom react hook that ignites socket connection by provided event types and returns [connect, disconnect, singleConnect, singleDisconnect]<br>
[x] Disable submit on all forms if validation hasn't passed<br>
[x] Modularize Redux store (after initial setup, pain in the ..)<br>
[x] Axios interceptor for each request (format response and error objects) and response (set authorization headers)<br>
[x] Implemented socket for email verification - i.e. when user verifies email, a notifcation pops up, and profile is automaticly updated (verification icon turns to check) and server generates new JWT (blacklists the old one) which is directly set to `localStorage`<br>
[x] Create a middleware for attaching initialized Socket to `res.locals.io` so it is available only on certain routes.<br>
[x] Universal editor for both questions and answers<br>
[x] Learn how to properly (?) stylize AntD components (great library btw)<br>
[x] Add rating system that allows user to add only one rating per question/answer (can be improved, will be)<br>
[x] Add Home page filters with load more functionality (server wide pagination middleware was created though only limit is used for this)<br>
[x] Add MyQuestions page for logged user questions with all details (latestQuestions, usersWithMostAnswers, questionsWithMostLikes)<br>
[x] Add multilingual i18n support using React useContext, useReducer, useState hooks. <br>

[x] Server has standardized responses in form of: <br>

```
{
  "status": "success",
  "message": "User friendly message",
  "data": [{}] || {}
}
```

[x] Server has differnt setup for each environment concerning Error messages, but they are standardize throughout the app nonetheless: <br>

```
{
  "status": "error",
  "message": "User friendly message",
  "error": Error,
  "httpCode" : 404,
  "details": [{}]
}
```

[x] Both responses have been directly connected with the notification system <br>
[x] Swagger docs following Open API spec, loaded on host `http://localhost:8080` <br>
[x] Postman collections for each available server route <br>

### Technologies

- docker-compose
- NodeJS ( Express )
- TypeScript
- PostgreSQL

### Services loaded with docker-compose

- NodeJs
- PGAdmin
- Swagger
- PGAdmin
- [] Redis

### Info

- Destructuring is used in many places ( even if it's not required ) to increase readability. Until everything is either JSDoc-ed or TypeScript-ed.
- Catching universal server errors are done in separate functions to achieve consistency with data display to the user.
  - Potential issue: stack tracing.
- I went with AntD to try out a new React UI library, since I'm aware of Material's customization issues. Now I see this one is no better at it. ( `!important` is temporary ).
- Though I've used a certain code-writing style that might be frowned upon, it was for my fun only, I'm flexible when it comes to styleguides.

### Todo

[x] Multilingual <br>
[] Separate controller from query stuff (create service for every route) <br>
[] LazyLoading <br>
[] Search with filters <br>
[] PropTypes <br>

[?] Fetch data before navigating to route <br>
[x] Additional client validation <br>
[x] JsDoc Redux actions <br>
[x] Prompt on delete user <br>
[x] Hook for Socket <br>
[] Remember password <br>

---

[] Universal `{ data: {}, params: {}}` parameter sent to all api calls? <br>
[] Swagger Docs according to Open API standard <br>
[] Postman Collections <br>
[] Set your errors straight <br>
[] Save error logs <br>

---

[] Enable image upload via AWS <br>
[] Deactivate user <br>
[] Refresh token <br>
[] Add friend <br>

### Experimental ?

- Tests ( Jest )
- Webpack
- GraphQL / Apollo
- Microservices (Auth & Mailer)
- React Native

### How to start the app

To start all app dependant containers simultanuously, one must have `docker-compose` installed.

App is started simply by navigating to root folder containing `docker-compose.yaml` file and staring docker-compose by entering `docker-compose up` in the terminal.

React app should be visible on port `http://localhost:3001`

Whoever is to test the app, environemnt variables will be given privately, though your own can be generated by looking them up in the `./server/env/.example.env` file or `./.example.env`

Enjoy!

